﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using App_Code.Controls;

public partial class widgets_FBpageLikeBox_widget : WidgetBase
{
    /// <summary>
    ///     Gets a value indicating if the header is visible. This only takes effect if the widgets isn't editable.
    /// </summary>
    /// <value><c>true</c> if [display header]; otherwise, <c>false</c>.</value>
    public override bool DisplayHeader
    {
        get
        {
            return true;
        }
    }

    /// <summary>
    ///     Gets wether or not the widget can be edited.
    ///     <remarks>
    ///         The only way a widget can be editable is by adding a edit.ascx file to the widget folder.
    ///     </remarks>
    /// </summary>
    /// <value></value>
    public override bool IsEditable
    {
        get
        {
            return true;
        }
    }

    /// <summary>
    ///     Gets the name. It must be exactly the same as the folder that contains the widget.
    /// </summary>
    /// <value></value>
    public override string Name
    {
        get
        {
            return "FBpageLikeBox";
        }
    }

    /// <summary>
    /// This method works as a substitute for Page_Load. You should use this method for
    ///     data binding etc. instead of Page_Load.
    /// </summary>
    public override void LoadWidget()
    {
        // Nothing to load
    }

    public string WidgetContent
    {
        get
        {
            var settings = this.GetSettings();
            //var ret = "<iframe src=\"http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2F{0}%2F{1}&amp;width={2}&amp;colorscheme=light&amp;show_faces={5}&amp;stream={6}&amp;header={4}&amp;height={3}\" scrolling=\"no\" frameborder=\"0\" style=\"border:none; overflow:hidden; width:246px; height:280px;\" allowTransparency=\"true\"></iframe>";
            var ret = "<iframe src=\"http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2F{0}&amp;width={2}&amp;colorscheme=light&amp;show_faces={5}&amp;stream={6}&amp;header={4}&amp;height={3}\" scrolling=\"no\" frameborder=\"0\" style=\"border:none; overflow:hidden; width:246px; height:280px;\" allowTransparency=\"true\"></iframe>";
            if (settings != null && settings["pagename"] != null)
            {
                bool showHeader;
                string sH = "true";
                string sF = "true";
                string sS = "false";

                if (bool.TryParse(settings["showheader"], out showHeader))
                {
                    sH = showHeader.ToString();
                }
                bool showFaces;
                if (bool.TryParse(settings["showfaces"], out showFaces))
                {
                    sF = showFaces.ToString();
                }
                bool showStream;
                if (bool.TryParse(settings["showstream"], out showStream))
                {
                    sS= showStream.ToString();
                }

                ret = string.Format(ret, settings["pagename"], settings["pagekey"], settings["width"], settings["height"], sH, sF, sS);
            }
            else
            {
                ret = "<iframe src=\"http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FBlog-Engine-Wall%2F201501709867816&amp;width=246&amp;colorscheme=light&amp;show_faces=true&amp;stream=false&amp;header=true&amp;height=280\" scrolling=\"no\" frameborder=\"0\" style=\"border:none; overflow:hidden; width:246px; height:280px;\" allowTransparency=\"true\"></iframe>";
            }
            return ret;
        }
    }
}