﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The admin_html editor.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Admin
{
    using System.Web.UI;

    /// <summary>
    /// The admin_html editor.
    /// </summary>
    public partial class HtmlEditor : UserControl
    {
        #region Properties

        /// <summary>
        /// Gets or sets TabIndex.
        /// </summary>
        public short TabIndex
        {
            get
            {
                return this.CKEditor1.TabIndex;
            }

            set
            {
                this.CKEditor1.TabIndex = value;
            }
        }

        /// <summary>
        /// Gets or sets Text.
        /// </summary>
        public string Text
        {
            get
            {
                return this.CKEditor1.Text;
            }

            set
            {
                this.CKEditor1.Text = value;
            }
        }

        #endregion
    }
}