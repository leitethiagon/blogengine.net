﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="edit.ascx.cs" Inherits="widgets_FBpageLikeBox_edit" %>

<label for="<%=txtPageName %>">Page Name</label><br />
<asp:TextBox runat="server" ID="txtPageName" Width="300" />
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="Server" ControlToValidate="txtPageName" ErrorMessage="Please enter a Page Name" Display="dynamic" /><br /><br />

<label for="<%=txtKey %>">Page Key</label><br />
<asp:TextBox runat="server" ID="txtKey" Width="300" />
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="Server" ControlToValidate="txtKey" ErrorMessage="Please enter a Page Key" Display="dynamic" /><br /><br />

<label for="<%=txtWidth %>">Width</label><br />
<asp:TextBox runat="server" ID="txtWidth" Width="30" /><br /><br />

<label for="<%=txtHeight %>">Height</label><br />
<asp:TextBox ID="txtHeight" runat="server" Width="30" /><br /><br />

<asp:CheckBox runat="Server" ID="cbShowHeader" Text=" Show Header" /><br />

<asp:CheckBox runat="Server" ID="cbShowFaces" Text=" Show Faces" /><br />

<asp:CheckBox runat="Server" ID="cbShowStream" Text=" Show Stream" /><br />