﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CKEditor.ascx.cs" Inherits="Admin.CKEditor" %>
<%@ Import Namespace="BlogEngine.Core" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<CKEditor:CKEditorControl ID="txtContent" runat="server"></CKEditor:CKEditorControl>