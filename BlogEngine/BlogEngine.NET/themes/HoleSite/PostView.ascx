﻿<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" Inherits="BlogEngine.Core.Web.Controls.PostViewBase" %>
<div class="post xfolkentry" id="post<%=Index %>">
    <div class="mao"></div>
    <div class="data"><span><%= Post.DateCreated.ToString("dd")%></span><br /><%= Post.DateCreated.ToString("MMMM")%><br /><%= Post.DateCreated.ToString("yyyy")%></div>
        <div class="titulo">
            <a href="<%=Post.RelativeLink %>" class="taggedlink"><%=Server.HtmlEncode(Post.Title) %></a>
        </div>
        <asp:PlaceHolder ID="BodyContent" runat="server" />
        <br />
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style ">
<a class="addthis_button_preferred_1"></a>
<a class="addthis_button_preferred_2"></a>
<a class="addthis_button_preferred_3"></a>
<a class="addthis_button_preferred_4"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4fdfccf43c6bf22e"></script>
<!-- AddThis Button END -->
        <div class="tags">Tags: <span><%=TagLinks(", ") %></span>
        <br />
        <%=AdminLinks %>
        <br />
        <%if (this.Location == BlogEngine.Core.ServingLocation.SinglePost)
          { %>
        Comentários:
        <fb:comments href="<%=Post.AbsoluteLink %>" num_posts="4" width="500"></fb:comments>
        <% }
          else
          { %>
          <fb:comments-count href=<%=Post.AbsoluteLink %>/></fb:comments-count> comentários
        <% } %>
    </div>
</div>