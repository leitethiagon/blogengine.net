﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using App_Code.Controls;

public partial class widgets_FBpageLikeBox_edit : WidgetEditBase
{
     #region Public Methods

        /// <summary>
        /// Saves this the basic widget settings such as the Title.
        /// </summary>
        public override void Save()
        {
            var settings = this.GetSettings();
            settings["pagename"] = this.txtPageName.Text;
            settings["pagekey"] = this.txtKey.Text;
            settings["width"] = this.txtWidth.Text;
            settings["height"] = this.txtHeight.Text;
            settings["showheader"] = this.cbShowHeader.Checked.ToString();
            settings["showfaces"] = this.cbShowFaces.Checked.ToString();
            settings["showstream"] = this.cbShowStream.Checked.ToString();
            this.SaveSettings(settings);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnInit(EventArgs e)
        {
            var settings = this.GetSettings();
            if (settings != null && settings["pagename"] != null)
            {
                this.txtPageName.Text = settings["pagename"];
                this.txtKey.Text = settings["pagekey"];
                this.txtWidth.Text = settings["width"];
                this.txtHeight.Text = settings["height"];

                bool showHeader;
                if (bool.TryParse(settings["showheader"], out showHeader))
                {
                    cbShowHeader.Checked = showHeader;
                }
                bool showFaces;
                if (bool.TryParse(settings["showfaces"], out showFaces))
                {
                    cbShowFaces.Checked = showFaces;
                }
                bool showStream;
                if (bool.TryParse(settings["showstream"], out showStream))
                {
                    cbShowStream.Checked = showStream;
                }
            }
            else
            {
                this.txtPageName.Text = "My Facebook Page";
                this.txtKey.Text = "1234567890";
                this.txtWidth.Text = "240";
                this.txtHeight.Text = "280";
                cbShowHeader.Checked = true;
                cbShowFaces.Checked = true;
            }
            base.OnInit(e);
        }

        #endregion
}