﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The admin tiny mce.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Admin
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Text;

    /// <summary>
    /// The admin tiny mce.
    /// </summary>
    public partial class CKEditor : UserControl
    {
        protected override void OnLoad(System.EventArgs e)
        {
            //txtContent.BaseHref = BlogEngine.Core.Utils.AbsoluteWebRoot.ToString();
            // have to set content path here otherwise it doesnt work
            txtContent.BasePath = BlogEngine.Core.Utils.AbsoluteWebRoot.ToString() + "editors/ckeditor";
            txtContent.config.toolbar = new object[]
			{
				new object[] { "Source", "-", "NewPage", "Preview", "-", "Templates" },
				new object[] { "Cut", "Copy", "Paste", "PasteText", "PasteFromWord", "-", "Print" },
				new object[] { "Undo", "Redo", "-", "Find", "Replace", "-", "SelectAll", "RemoveFormat" },
				"/",
				new object[] { "Bold", "Italic", "Underline", "Strike", "-", "Subscript", "Superscript" },
				new object[] { "NumberedList", "BulletedList", "-", "Outdent", "Indent", "Blockquote", "CreateDiv" },
				new object[] { "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock" },
				new object[] { "BidiLtr", "BidiRtl" },
				new object[] { "Link", "Unlink", "Anchor" },
				new object[] { "Image", "Flash", "Table", "HorizontalRule", "Smiley", "SpecialChar", "PageBreak", "Iframe" },
				"/",
				new object[] { "Styles", "Format", "Font", "FontSize" },
				new object[] { "TextColor", "BGColor" },
				new object[] { "Maximize", "ShowBlocks", "-", "About" }
			};

            base.OnLoad(e);
        }

        #region Properties

        /// <summary>
        /// Gets or sets Height.
        /// </summary>
        public Unit Height
        {
            get
            {
                return this.txtContent.Height;
            }

            set
            {
                this.txtContent.Height = value;
            }
        }

        /// <summary>
        /// Gets or sets TabIndex.
        /// </summary>
        public short TabIndex
        {
            get
            {
                return this.txtContent.TabIndex;
            }

            set
            {
                this.txtContent.TabIndex = value;
            }
        }

        /// <summary>
        /// Gets or sets Text.
        /// </summary>
        public string Text
        {
            get
            {
                return this.txtContent.Text;
            }

            set
            {
                this.txtContent.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets Width.
        /// </summary>
        public Unit Width
        {
            get
            {
                return this.txtContent.Width;
            }

            set
            {
                this.txtContent.Width = value;
            }
        }

        #endregion
    }
}